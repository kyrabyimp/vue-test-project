interface NavLink {
    url: string,
    name: string
}

export default NavLink